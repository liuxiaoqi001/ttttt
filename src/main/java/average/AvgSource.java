package average;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.ShuffleConsumerPlugin;


import java.util.StringTokenizer;


public class AvgSource {
    public static class AvgSourceMap extends Mapper<LongWritable, Text,Text, IntWritable>{
        protected void map(LongWritable key, Text value, ShuffleConsumerPlugin.Context context)
            throws java.io.IOException,InterruptedException{
            String line = value.toString();
            StringTokenizer tok = new StringTokenizer(line,"\n");
            while (tok.hasMoreElements()){
                String name = tok.nextToken();
                String source =tok.nextToken();
                context.write(new Text(name),new IntWritable(Integer.parseInt(source)));
            }

        }
    }
}
