package com.etc;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;


public class JobSubmit {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration configuration = new Configuration();


        Job job = Job.getInstance(configuration);

        job.setJarByClass(JobSubmit.class);

        //设置mapper类是哪个
        job.setMapperClass(WordCountMapper.class);
        //设置reduce类是哪个
        job.setReducerClass(WordCountReduce.class);

        //mapper  输出key
        job.setMapOutputKeyClass(Text.class);
        //mapper  输出value
        job.setMapOutputValueClass(IntWritable.class);

        //reduce  输出key
        job.setOutputKeyClass(Text.class);
        //reduce  输出value
        job.setOutputValueClass(IntWritable.class);


        //输入路径
        FileInputFormat.setInputPaths(job,new Path("F:\\a.txt"));
        //输出路径 (注意输出路径文件夹必须不存在)
        FileOutputFormat.setOutputPath(job,new Path("F:\\output"));


        // 5、封装参数：想要启动的reduce task的数量
        job.setNumReduceTasks(1);

        // 6、提交job给yarn
//        boolean res = job.waitForCompletion(true);

        System.exit(job.waitForCompletion(true)?0:1);
    }


    }

