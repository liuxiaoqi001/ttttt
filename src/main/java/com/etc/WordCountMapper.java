package com.etc;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


import java.io.IOException;



    //    * 第一个泛型：  文件偏移量
   //     * 第二个泛型：  文件的行
     //   * 第三个泛型：  map的key输出
       // * 第四个泛型：  map的value输出

public class WordCountMapper extends Mapper<LongWritable, Text,Text, IntWritable> {
@Override
    protected void map(LongWritable key,Text value,Context context) throws IOException,InterruptedException{
    //文件的第一行数据  hello wangcc
    String line = value.toString();
    //切单词 {"hello","wangcc"}
    String[] s = line.split( " ");
    for (String s1 : s){
        // hello  1
        context.write(new Text(s1),new IntWritable(1));
    }

}




}
